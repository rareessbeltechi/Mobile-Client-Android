package com.example.ilazar.notes.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ilazar.notes.R;
import com.example.ilazar.notes.viewmodel.NotesViewModel;
import com.example.ilazar.notes.vo.Note;

import java.util.List;

public class NoteListFragment extends Fragment {

    private NotesViewModel mNotesViewModel;
    private RecyclerView mNoteList;

    public static NoteListFragment newInstance() {
        return new NoteListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.note_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNoteList = view.findViewById(R.id.note_list);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mNoteList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mNotesViewModel = ViewModelProviders.of(this).get(NotesViewModel.class);
        mNotesViewModel.getNotes().observe(this, new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {
                NoteListAdapter notesAdapter = new NoteListAdapter(getActivity(), notes);
                mNoteList.setAdapter(notesAdapter);
            }
        });

    }

}

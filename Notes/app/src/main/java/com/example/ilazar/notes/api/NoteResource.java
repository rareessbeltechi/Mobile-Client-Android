package com.example.ilazar.notes.api;

import com.example.ilazar.notes.vo.Page;

import retrofit2.Call;
import retrofit2.http.GET;

public interface NoteResource {
    String BASE_URL = "http://10.198.219.145:3000/";

    @GET("note")
    Call<Page> getNotes();
}

package com.example.ilazar.notes.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.ilazar.notes.api.NoteResource;
import com.example.ilazar.notes.vo.Note;
import com.example.ilazar.notes.vo.Page;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NotesViewModel extends ViewModel {
    private static final String TAG = NotesViewModel.class.getCanonicalName();
    private MutableLiveData<List<Note>> notes;

    public LiveData<List<Note>> getNotes() {
        if (notes == null) {
            notes = new MutableLiveData<List<Note>>();
            loadNotes();
        }
        return notes;
    }

    private void loadNotes() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NoteResource.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NoteResource api = retrofit.create(NoteResource.class);
        Call<Page> call = api.getNotes();
        Log.d(TAG, "loadNotes");
        call.enqueue(new Callback<Page>() {
            @Override
            public void onResponse(Call<Page> call, Response<Page> response) {
                Log.d(TAG, "loadNotes succeeded");
                notes.setValue(response.body().getNotes());
            }

            @Override
            public void onFailure(Call<Page> call, Throwable t) {
                Log.e(TAG, "loadNotes failed", t);
            }
        });
    }

}

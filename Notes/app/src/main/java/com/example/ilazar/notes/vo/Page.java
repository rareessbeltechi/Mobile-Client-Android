package com.example.ilazar.notes.vo;

import java.util.List;

public class Page {
    private int number;
    private List<Note> notes;

    public int getNumber() {
        return number;
    }

    public List<Note> getNotes() {
        return notes;
    }

    @Override
    public String toString() {
        return "Page{" +
                "number=" + number +
                ", notes=" + notes +
                '}';
    }
}

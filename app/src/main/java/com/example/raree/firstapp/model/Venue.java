package com.example.raree.firstapp.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Venue")
public class Venue {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name="id")
    @SerializedName("_id")
    private String id;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    private String name;


    @ColumnInfo(name = "description")
    @SerializedName("description")
    private String description;


    @ColumnInfo(name = "urlImg")
    @SerializedName("urlImg")
    private String urlImg;

    @Ignore
    @SerializedName("createdAt")
    private String createdAt;

    @Ignore
    @SerializedName("updatedAt")
    private String updatedAt;

    @Ignore
    public Venue(String id, String name, String description, String urlImg, String createdAt, String updatedAt) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.urlImg = urlImg;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Venue() {
    }

    public Venue(String id, String name, String description, String urlImg) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.urlImg = urlImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}

package com.example.raree.firstapp.ApiService;

import com.example.raree.firstapp.model.Venue;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface VenueApiService {
    @GET("venues")
    Call<List<Venue>> getAllVenues(@Header("Authorization") String token);
}

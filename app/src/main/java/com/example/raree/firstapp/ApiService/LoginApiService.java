package com.example.raree.firstapp.ApiService;

import com.example.raree.firstapp.model.Token;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginApiService {

    @POST("/auth/sign_in")
    @FormUrlEncoded
    Call<Token> sign_in(@Field("email") String email,
                        @Field("password") String password);
}

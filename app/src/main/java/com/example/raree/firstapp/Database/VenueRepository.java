package com.example.raree.firstapp.Database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.raree.firstapp.DAO.VenueDAO;
import com.example.raree.firstapp.model.Venue;

import java.util.List;

public class VenueRepository {
    private VenueDAO venueDAO;
    private LiveData<List<Venue>> allVenues;

    public VenueRepository(Application application) {
        VenueDatabase database = VenueDatabase.getInstance(application);
        venueDAO = database.venueDAO();
        allVenues = venueDAO.getAllVenue();
    }

    public void insert(Venue venue){
        new InsertVenueAsyncTask(venueDAO).execute(venue);
    }

    public void update(Venue venue){
        new UpdateVenueAsyncTask(venueDAO).execute(venue);
    }

    public void delete(Venue venue){
        new DeleteVenueAsyncTask(venueDAO).execute(venue);
    }

    public void deleteAllVenues(){
        new DeleteAllVenueAsyncTask(venueDAO).execute();
    }

    public LiveData<List<Venue>> getAllVenue(){
        return allVenues;
    }

    private static class InsertVenueAsyncTask extends AsyncTask<Venue,Void,Void>{

        private VenueDAO venueDAO;

        private InsertVenueAsyncTask(VenueDAO venue){
            this.venueDAO = venue;
        }

        @Override
        protected Void doInBackground(Venue... venues) {
            this.venueDAO.insert(venues[0]);
            return null;
        }
    }

    private static class UpdateVenueAsyncTask extends AsyncTask<Venue,Void,Void>{

        private VenueDAO venueDAO;

        private UpdateVenueAsyncTask(VenueDAO venue){
            this.venueDAO = venue;
        }

        @Override
        protected Void doInBackground(Venue... venues) {
            this.venueDAO.update(venues[0]);
            return null;
        }
    }
    private static class DeleteVenueAsyncTask extends AsyncTask<Venue,Void,Void>{

        private VenueDAO venueDAO;

        private DeleteVenueAsyncTask(VenueDAO venue){
            this.venueDAO = venue;
        }

        @Override
        protected Void doInBackground(Venue... venues) {
            this.venueDAO.delete(venues[0]);
            return null;
        }
    }
    private static class DeleteAllVenueAsyncTask extends AsyncTask<Void,Void,Void>{

        private VenueDAO venueDAO;

        private DeleteAllVenueAsyncTask(VenueDAO venue){
            this.venueDAO = venue;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            this.venueDAO.deleteAllVenues();
            return null;
        }
    }
}

package com.example.raree.firstapp;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.raree.firstapp.ApiService.LoginApiService;
import com.example.raree.firstapp.ApiService.RetrofitFOClientInstance;
import com.example.raree.firstapp.Globals.Globals;
import com.example.raree.firstapp.ListItems.ListItems;
import com.example.raree.firstapp.VM.UserVM;
import com.example.raree.firstapp.model.Token;
import com.example.raree.firstapp.model.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Button loginButton;
    private EditText usernameText;
    private EditText passwordText;
    private ProgressBar progressBar;
    private UserVM userVM;
    private List<User> usersFromDB ;
    Globals globals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loginButton = findViewById(R.id.button_login_login);
        usernameText = findViewById(R.id.editText_login_username);
        passwordText = findViewById(R.id.editText_login_password);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        globals = Globals.getInstance();
        userVM = ViewModelProviders.of(this).get(UserVM.class);
        userVM.getAllUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {
                usersFromDB = users;
            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                if (isOnline()) {
                    sign_in();
                } else {
                    toastMessage("is not online");
                    sign_in_local();
                }
            }
        });
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void sign_in() {
        LoginApiService loginApiService = RetrofitFOClientInstance.getRetrofitInstance().create(LoginApiService.class);

        Call<Token> call = loginApiService.sign_in(usernameText.getText().toString(), passwordText.getText().toString());
        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                Token token = response.body();

                try {
                    if (token.getToken().contains("Authentication failed")) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(MainActivity.this, "Authentication failed...", Toast.LENGTH_SHORT).show();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        globals.setToken("JWT " + token.getToken());
                        userVM.insert(new User(usernameText.getText().toString(), passwordText.getText().toString()));
                        Intent listItem = new Intent(MainActivity.this, ListItems.class);
                        startActivity(listItem);
                    }
                } catch (NullPointerException e) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this, "Authentication failed...", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Something went wrong...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void sign_in_local() {
        User user = null;
        for(int i = 0 ; i < usersFromDB.size();i++){
            if(usernameText.getText().toString().equals(usersFromDB.get(i).getUsername()) &&  passwordText.getText().toString().equals(usersFromDB.get(i).getPassword())){
                user = usersFromDB.get(i);
            }
        }
        if (user == null) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, "Authentication failed...", Toast.LENGTH_SHORT).show();
        } else {
            progressBar.setVisibility(View.GONE);
            Intent listItem = new Intent(MainActivity.this, ListItems.class);
            startActivity(listItem);
        }
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}

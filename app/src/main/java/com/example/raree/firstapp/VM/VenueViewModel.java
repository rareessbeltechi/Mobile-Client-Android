package com.example.raree.firstapp.VM;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.raree.firstapp.R;
import com.example.raree.firstapp.model.Venue;
import com.squareup.picasso.Picasso;

import java.util.List;

public class VenueViewModel extends RecyclerView.Adapter<VenueViewModel.VenueViewHolder> {

    private List<Venue> venueList;

    public VenueViewModel(List<Venue> venues) {
        this.venueList = venues;
    }

    @NonNull
    @Override
    public VenueViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cardView = (CardView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_layout, viewGroup, false);
        VenueViewHolder venueViewHolder = new VenueViewHolder(cardView);
        return venueViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VenueViewHolder holder, int i) {

        ImageView imageView;
        TextView customerName;
        TextView customerDescription;
        imageView = (ImageView) holder.cardView.findViewById(R.id.custmer_urlImg);
        customerName = (TextView) holder.cardView.findViewById(R.id.customer_name);
        customerDescription = (TextView) holder.cardView.findViewById(R.id.customer_description);
        Picasso.get().load(venueList.get(i).getUrlImg()).into(imageView);
        customerName.setText(venueList.get(i).getName());
        customerDescription.setText(venueList.get(i).getDescription());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                Intent intent = new Intent(v.getContext(),MainActivity.class);
//                v.getContext().startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return this.venueList.size();
    }

    class VenueViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;

        VenueViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
        }
    }

}

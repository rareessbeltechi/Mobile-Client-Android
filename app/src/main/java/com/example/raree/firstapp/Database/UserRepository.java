package com.example.raree.firstapp.Database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.raree.firstapp.DAO.UserDAO;
import com.example.raree.firstapp.model.User;

import java.util.List;

public class UserRepository {
    private UserDAO userDAO;
    private LiveData<List<User>> allUsers;
    public UserRepository(Application application){
        UserDatabase userDatabase = UserDatabase.getInstance(application);
        userDAO = userDatabase.userDAO();
        allUsers = userDAO.getAllUsers();
    }

    public void insert(User user){
        new InsertUserAsyncTask(userDAO).execute(user);
    }

    public LiveData<List<User>> getAllUsers(){
        return allUsers;
    }

    private static class InsertUserAsyncTask extends AsyncTask<User,Void,Void> {

        private UserDAO userDAO;

        private InsertUserAsyncTask(UserDAO userdao){
            this.userDAO = userdao;
        }

        @Override
        protected Void doInBackground(User... users) {
            this.userDAO.insert(users[0]);
            return null;
        }
    }
}

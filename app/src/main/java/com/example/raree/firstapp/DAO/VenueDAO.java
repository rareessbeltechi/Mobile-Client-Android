package com.example.raree.firstapp.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.raree.firstapp.model.Venue;

import java.util.List;

@Dao
public interface VenueDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Venue venue);

    @Update
    void update(Venue venue);

    @Delete
    void delete(Venue venue);

    @Query("DELETE FROM Venue")
    void deleteAllVenues();

    @Query("SELECT * FROM Venue ")
    LiveData<List<Venue>> getAllVenue();

}

package com.example.raree.firstapp.Database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.raree.firstapp.DAO.VenueDAO;
import com.example.raree.firstapp.model.Venue;

@Database(entities = Venue.class , version = 2)
public abstract class VenueDatabase extends RoomDatabase {

    private static VenueDatabase instance;

    public abstract VenueDAO venueDAO();

    public static synchronized VenueDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),VenueDatabase.class,"Venue")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDBAsyncTask(instance).execute();
        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
        }
    };

    private static class PopulateDBAsyncTask extends AsyncTask<Void,Void,Void>{
        private VenueDAO venueDAO;

        private PopulateDBAsyncTask(VenueDatabase venueDao){
            this.venueDAO = venueDao.venueDAO();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            venueDAO.insert(new Venue("432432","Rares Test1","Daa","https://fastorder-parse-prod-bucket.s3.amazonaws.com/01d50369b94046a932e219d24a3dcbb2_logo_uncle.png",null,null));
            venueDAO.insert(new Venue("4324325","Rares Test2","Daa","https://fastorder-parse-prod-bucket.s3.amazonaws.com/01d50369b94046a932e219d24a3dcbb2_logo_uncle.png",null,null));
            venueDAO.insert(new Venue("4324326","Rares Test3","Daa","https://fastorder-parse-prod-bucket.s3.amazonaws.com/01d50369b94046a932e219d24a3dcbb2_logo_uncle.png",null,null));

            return null;
        }
    }
}

package com.example.raree.firstapp.ListItems;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.solver.GoalRow;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.raree.firstapp.ApiService.RetrofitFOClientInstance;
import com.example.raree.firstapp.ApiService.VenueApiService;
import com.example.raree.firstapp.Database.VenueRepository;
import com.example.raree.firstapp.Globals.Globals;
import com.example.raree.firstapp.R;
import com.example.raree.firstapp.VM.VenueVM;
import com.example.raree.firstapp.VM.VenueViewModel;
import com.example.raree.firstapp.model.Venue;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListItems extends AppCompatActivity {

    private VenueViewModel venueViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    ProgressDialog progressDialog;
    SwipeRefreshLayout swipeRefreshLayout;
    private VenueVM venueVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_items);

        swipeRefreshLayout = findViewById(R.id.Swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isOnline()){
                    venueVM.deleteAllVenues();
                    getVenues();
                }
                else{
                    toastMessage("is not online");
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                },4000);
            }
        });

        venueVM = ViewModelProviders.of(this).get(VenueVM.class);
        venueVM.getAllVenues().observe(this, new Observer<List<Venue>>() {
            @Override
            public void onChanged(@Nullable List<Venue> venues) {
                progressDialog.dismiss();
                venueViewModel = new VenueViewModel(venues);

                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(venueViewModel);
            }
        });
        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        progressDialog = new ProgressDialog(ListItems.this);
        progressDialog.setMessage("Loading .....");
        progressDialog.show();

        if(isOnline()){
            venueVM.deleteAllVenues();
            getVenues();
        }

    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
    private void toastMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
    public void getVenues(){
        VenueApiService service = RetrofitFOClientInstance.getRetrofitInstance().create(VenueApiService.class);
        Globals globals = Globals.getInstance();
        Call<List<Venue>> call = service.getAllVenues(globals.getTokenFromGlobals());
        call.enqueue(new Callback<List<Venue>>() {
            @Override
            public void onResponse(Call<List<Venue>> call, Response<List<Venue>> response) {

                List<Venue> list = response.body();
                for(int i= 0 ; i < list.size() ; i++){
                    venueVM.insert(list.get(i));
                }


            }

            @Override
            public void onFailure(Call<List<Venue>> call, Throwable t) {
                progressDialog.dismiss();
                toastMessage("Something went wrong....");
            }
        });
    }
}

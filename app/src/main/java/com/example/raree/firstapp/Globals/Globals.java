package com.example.raree.firstapp.Globals;

public class Globals {

    private static Globals instance;
    private String token;

    private Globals() {

    }

    public void setToken(String token){
        this.token = token;
    }

    public String getTokenFromGlobals(){
        return this.token;
    }

    public static synchronized Globals getInstance(){
        if(instance == null)
            instance = new Globals();
        return instance;
    }
}

package com.example.raree.firstapp.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.raree.firstapp.DAO.UserDAO;
import com.example.raree.firstapp.DAO.VenueDAO;
import com.example.raree.firstapp.model.User;
import com.example.raree.firstapp.model.Venue;

@Database(entities = {User.class, Venue.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "Venues.db";

    private static volatile AppDatabase instance;

    private static final Object LOCK = new Object();

    public abstract UserDAO userDAO();

    public abstract VenueDAO venueDAO();

    public static AppDatabase getInstance(Context context) {
        if(instance == null){
            synchronized (LOCK) {
                if (instance == null){
                    instance = Room
                            .databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return instance;
    }

}

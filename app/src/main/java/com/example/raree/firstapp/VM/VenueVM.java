package com.example.raree.firstapp.VM;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.raree.firstapp.Database.VenueRepository;
import com.example.raree.firstapp.model.Venue;

import java.util.List;

public class VenueVM extends AndroidViewModel {

    private VenueRepository repository;
    private LiveData<List<Venue>> allVenues;

    public VenueVM(@NonNull Application application) {
        super(application);
        repository = new VenueRepository(application);
        allVenues = repository.getAllVenue();
    }

    public void insert(Venue venue){
        repository.insert(venue);
    }

    public void update(Venue venue){
        repository.update(venue);
    }

    public void delete(Venue venue){
        repository.delete(venue);
    }

    public void deleteAllVenues(){
        repository.deleteAllVenues();
    }

    public LiveData<List<Venue>> getAllVenues(){
        return allVenues;
    }
}

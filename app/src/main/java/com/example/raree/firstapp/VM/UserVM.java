package com.example.raree.firstapp.VM;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.raree.firstapp.Database.UserRepository;
import com.example.raree.firstapp.model.User;

import java.util.List;

public class UserVM extends AndroidViewModel {

    private UserRepository repository;
    private LiveData<List<User>> allUsers;

    public UserVM(@NonNull Application application) {
        super(application);
        repository = new UserRepository(application);
        allUsers = repository.getAllUsers();
    }

    public void insert(User user){
        repository.insert(user);
    }

    public LiveData<List<User>> getAllUsers(){
        return allUsers;
    }
}
